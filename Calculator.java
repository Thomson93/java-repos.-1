import java.util.Scanner;

public class Calculator {
	private static Scanner userinput;
	
	public static void main(String[] args) {
		boolean loop = true;
		
		System.out.println("1. Addition. 2. Subtraction. 3. Multiplication. 4. Division. 5. Sin. 6. Cos. 7. Tan. Type quit to exit.");
		System.out.println("Examples: \n\t1, hit enter. Then type 2 + 4. \n\t7, hit enter. Then type tan 43.");
		
		while (loop == true) {
		userinput = new Scanner(System.in);

		String text = userinput.nextLine();

		/// Need to add more methods. Right now this won't work if I add a GUI.
		
		switch (text) {
		case "1":
			double add1 = 0;
			add1 = userinput.nextDouble();
			
			String add3 = " + ";
			add3 = userinput.next();
			
			double add2 = 0;
			add2 = userinput.nextDouble();
			System.out.println(add1 + add2);
			break;

		case "2":
			double minus1 = 0;
			minus1 = userinput.nextDouble();
			
			String minus3 = " - ";
			minus3 = userinput.next();
			
			double minus2 = 0;
			minus2 = userinput.nextDouble();
			System.out.println(minus1 - minus2);
			break;
			
		case "3":
			double mult1 = 0;
			mult1 = userinput.nextDouble();
			
			String mult3 = " x ";
			mult3 = userinput.next();
			
			double mult2 = 0;
			mult2 = userinput.nextDouble();
			System.out.println(mult1 * mult2);
			break;
			
		case "4":
			double divi1 = 0;
			divi1 = userinput.nextDouble();
			
			String divi3 = " / ";
			divi3 = userinput.next();
			
			double divi2 = 0;
			divi2 = userinput.nextDouble();
			System.out.println(divi1 / divi2);
			break;
			
		case "5":
			
			String sine1 = "sin ";
			sine1 = userinput.next();
			
			double sine2 = 0;
			sine2 = userinput.nextDouble();
			
			System.out.println(Math.sin((Math.PI/180) * sine2));
			break;
			
		case "6":
			
			String cosi1 = "cos ";
			cosi1 = userinput.next();
			
			double cosi2 = 0;
			cosi2 = userinput.nextDouble();
			
			System.out.println(Math.cos((Math.PI/180) * cosi2));
			break;
			
		case "7":
			
			String tan1 = "tan ";
			tan1 = userinput.next();
			
			double tan2 = 0;
			tan2 = userinput.nextDouble();
			
			System.out.println(Math.tan((Math.PI/180) * tan2));
			break;
			
		case "quit":
			loop = false;
			System.out.println("Quitting Program");
			break;
			
		default:
			System.out.println("Command not recognised.");
			}
			
		
		if(loop == true) {
			System.out.println("1. Addition. 2. Subtraction. 3. Multiplication. 4. Division. 5. Sin. 6. Cos. 7. Tan. Type quit to exit.");
				}

			}
		
		}

	}